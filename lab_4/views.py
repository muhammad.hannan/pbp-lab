from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm
from django.http.response import HttpResponseRedirect

# Create your views here.

def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context ={}
    form = NoteForm(request.POST or None, request.FILES or None)
      
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/lab-3')
  
    context['form']= form
    return render(request, 'lab4_form.html', context)

def note_list(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)